#  Ironman - Diablo II LoD Mod

Ironman is a **hardcore-only** mod based heavily on the Ironman style of game play, and as such imposes many rules and limits on what can or cannot be done in-game. The mod however does not fully comply with Ironman gameplay, as NPC interaction is allowed, hirelings can be hired... and so forth.

Hardcore-only means that every character created by default is in Diablo II hardcore mode. The ability to create softcore characters is gone. Hardcore is the only real way to play Ironman as it tests your abilities to the limits.

Here's some info about the mod. I don't want to give everything away. Play the mod to discover its awesomeness.

![Ironman Mod](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/TitleScreen.png)
![Ironman Mod](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/CharSelect.png)
![Ironman Mod](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/LoadChar.png)
![Ironman Mod](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/Quest.png)
![Ironman Mod](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/Skills.png)
![Ironman Mod](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/Stash.png)
![Ironman Mod](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/Town.png)
![Ironman Mod](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/Stats.png)

#Experimental Items (Only in beta version, not in public version. For testing crafting ideas.)

![Ironman Mod](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/NPC1.png)
![Ironman Mod](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/NPC2.png)
![Ironman Mod](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/NPC3.png)

![Ironman Mod](https://i.imgur.com/kwC22tb.png)
![Ironman Mod](https://i.imgur.com/39p5rQ1.png)
![Ironman Mod](https://i.imgur.com/pZ0CLSb.png)
![Ironman Mod](https://i.imgur.com/caqDFep.png)
![Ironman Mod](https://i.imgur.com/kwC22tb.png)

# Lots of new Gems (Play to discover what they do!)
![Ironman Mod](https://i.imgur.com/hacIXkw.png)

#  Magic Essence Harvester!

Take all the junk magic, rare, set, or unique items in the game and store their magic essense in the Harvester. This will destroy the item in question. You can then use Essence Points (EPs) to create all kinds of magic items and properties.

![Magic Essence Harvester](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/MagicHarvester.jpg)

## Harvester Items
Use the Magic Harvested from items to create new items

## Rare Shard
![Rare Shard](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/RareShard.jpg)

# Set Stone
![Set Stone](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/SetStone.jpg)

# Unique Particle
![UniqeParticle](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/UniqueParticle.jpg)

#  Gem Bag 
Store all your gems in one 1x2 gembag. In Ironman, inventory space is everything.
![Gem Bag](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/Gembag.jpg)


#  Rune Bag
Store all your gems in one 1x2 runebag. In Ironman, inventory space is everything.
![Rune Bag](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/Runebag.jpg)


#  Magic & Rare Arrows and Charms
Rare & Magic Arrows
![Rare & Magic Arrows](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/MagicArrows.jpg)


------------------------------------------

 
# What is the Ironman style of Diablo II Gameplay?
The Ironman style of gameplay is radically different from what you may have experienced.

 - You start with nothing.
 - You can buy nothing.
 - You can repair nothing.
 - Everything you use will come from the 'land'.
 
Diablo II's economic system was woefully lacking. This is remedied by the fact that gold has little to no use in the game. 

- Monsters won't drop gold. 
- You cannot hold gold in inventory or stash. 
- You by for 0 gold. 
- You sell for 0 gold. 

You may find a random pile here or there. That's okay. You can't pick it up anyway. 
  
***Town Portals are non-existent.*** Every time you want to go to town, you will have to walk or run to a waypoint. This will change the way you play the game drastically. Now the waypoints will actually be useful, instead of just being shortcuts to different levels, they will be your sole means for inter-level travel.  
  
***Item/Inventory management skills become a real necessity in Ironman.*** Your favorite weapon must be carefully used. Once it is gone, it is gone. Durability was lowered (though not greatly) to make more items in the game useful. 

***Players should not get attached to certain items and their properties or bonuses.*** With monster modification, it will require you to have different items and strategies for different monsters. Durability can be increased or repaired with moderately expensive yet balanced cube formulae.  
  
This new gameplay also requires changes to items, skills, monsters, and balancing! Those are carefully implemented and will be balanced over time as the need arises.  
  
# Gameplay:  
  
RUN! Seriously. Unlike Battle.net where you put on the best gear and dive into a pack of monsters which makes the gameplay stale and boring, almost every problem in Ironman can be handled by running. 

The only time you will find yourself in serious trouble is when a boss ambushes you with Holy Freeze as an Aura, or come up against a pack with of monsters who are 'Fast'. 

When in doubt, put distance between you and that pack of monsters. Let them come to you one or two at a time and deal with them. There is no shame in ***falling back!***  
  
Pick up everything. You have an increased backpack. Your stash is larger. If you think you can use it, put it into your pack. Stash it when you get to town. You will need it later, trust me.

With Yohann's PlugY, you will never run out of stash space. This plugin provides 'infinite' pages in the stash. You will often find yourself running with tons of potions. This is completely normal. Don't be embarrassed. Potions will only be available through game drops.
  
As much fun as those tiny little charms are, they become a burden when you realize that all of those bonuses don't make up for your lack of space in your inventory. Use them, get rid of them, see what they do in your cube.  
  
# Watch Your Inventory! 

Nothing will give you more grief than to have that nice set of armor beat completely off your body by high damage HTH monsters. Keep a set or two in your backpack and when necessary, change it out to reflect the situation. 

The same goes for weapons. You will undoubtedly find a nice weapon that can inflict massive damage. You will cry when you watch it break because you didn't use it conservatively. 

Use regular/magic weapons for standard monster-bashing. Use your Uber-Gear when you are about to fight the big boss and his minions. You have two weapons slots, use them. (Amazon gets off easy here, as does Sorceress.) 

*Just like it should be in RPGs, you intelligently decide which gear to use for which monster. You will find yourself running with two sets of gear, one for normal monsters, one for bosses, and you will be using the 'W' switch key very often.*
  
# Protect Your Hireling!

If you like to use them, get used to helping them. If you thought you carried lots of potions before, wait till you have to nursemaid them around! 

And because once dead, they are impossible to hire, it is in your (and their) best interest to keep them alive. Remember, you cannot Town-Portal to pull them out of a dangerous situation. If they are in trouble, you had better wade in and rescue them, or hope they weren't carrying good gear when they go down.

Hirelings can be hired for 0 gold at any time. The penalty is that your hireling will lose any items you put on them. Better make sure they are safe and help them not die.

Hirelings will be balanced and adjusted to make them better. Hire costs will also get lowered. Hireling can now use belts, boots, gloves, rings, and amulets in addition to the helm, weapon, and armor.

# Ironman Modifications
  
***Skills.*** Until full skill changes go into effect, you will come to appreciate the underused skills. Think of new and interesting ways to employ them. Nothing makes an Ironman multiplayer party happier than a Paladin with High-Level Prayer.  Seriously. 
  
***Stats.*** No matter what your class, Vitality becomes very important. Not only for the life but the Stamina as well. Enough said.  You have more stats per level up. Use them wisely.
  
***Gems.*** You will develop your own opinion about them. Save them for higher gems? Recipes? Or use them and lose them? The choice is yours. 
  
***Keep your way-points in mind.*** Know where the last one was. If you have to fall back and dash up to town, you are going to have to go down the previous WP and work your way back. 
 
When playing in MultiPlayer, make sure you can work as a team. This cannot be stressed enough. It's pretty sad to see two or three people arguing over a regular, non-magical Claymore in Act I. It is also really funny to watch a party fight over a low-quality cracked item (I've seen it happen!) Ironman is fun! :)
  
Even though your Assassin is best when Clawing monsters to death, there might be a time when that Rare Bow you just found can be very effective. ***Don't be afraid to try weapons that aren't the 'norm' for your class.*** Eagle Orbs have low durability, and losing a real nice one because you weren't paying attention serves you right!  
  
Be careful when using the 'Players X' command. Although it is tempting, you will regret it when you spawn a horrendous pack of GoreBelly's and cannot get through them because of their HP's last longer than your weapons and armor.  
  
#  Hardcore Ironman

Every character created is Hardcore by default.

The ultimate way to play IM. Become the master of falling back. Potion and item management skills increase tenfold in importance. Be conservative. Especially after you have clawed your way up a few levels.  
  
#  Current List of Changes (incomplete list. Play the mod to discover new features.)
 
 - D2mod.dll system in place with a lot of plugins.
 - Resist & damage caps set to 75% & 95% 
 - No gold lost on kill, No magic find cap
 - Implemented Yohann's PlugY 9
 - Mercenary inventory, Mega Inventory stash
 - Enable runewords for nrm,hi,mag,set,rar,uni,crf  
 - Maximum "/players X" value allowed is 8
 - Max items in gamble screen set to 1
 - You are not allowed to store any gold in the stash (it will not work)
 - You are not allowed to store any gold in the inventory (it will not work)

# New characters start with: 

 - Horadric Cube
 - Magic Essence Harvester
 - Rune Bag
 - Gem Bag
  
# Armor/Weapons/Misc
  
 - NPC will never spawn Armors/Weapons/Potions of any kind.
 - NPC will never spawn TP/ID scrolls or books.
 - Changed all item prices to 1 gold
 - Gheed will only have 1 ring in gamble window
 - Charsi sells Gembags and gem extractors 
 - Akara sells Runebags and rune extractors
 - Lowered item durability across the board
 - Added/changed level requirements for many items
 
 Added Sockets to all armors:
 - Gloves/Weapons. 
 - Belt/Armor. 
 - Boots/Armor

Added Sockets

 - Charms
 - Rings
 - Amulets
 - Arrows
 - Bolts

Added Elemeental Bolts & Arrows with Magic & Rare properties
    
# NPC    
 - NPC sells for 0 gold.
 - NPC buys for 0 gold.
 - Hireling hire cost is 0 gold.
 - Quest rewards concerning vendor prices have been disabled. 
  
# Item Drops
  
- Monsters will never drop gold
- Monsters will never drop TP/ID scrolls & TP Books 
- Better and more balanced item drops.
- Better rune drops. 
- You cannot repair items (expensive recipes will be added to repair durability. Don't get attached to items. Use whatever you get from the land, and enjoy the variable builds).


# Additional Resources:
- Complete Plugin Kit, TalonRage (Fixed/modified)
- Gembags Plugin, Xenast
- Item Graphics, Inventharia, Incandecence
- Map plugins, Diabolic Cartography
- D2Mod plugin system, Necrolis
- PlugY, Yohann
- D2SE Mod Manager, Snej
- Diabolic Cartography for Map plugins (Countess, Crypts, Act1 walls, Lut Gholein)
- Necrolis
- Nefarious
- Nizari
- Myhrginoc.
- SVR for the D2Mod System
- Dav92 for D2ExpRes.dll
- Revan for D2tweaks.dll
- Especially `andsimo' and the whole DIABLO MODS Discord community. You guys are awesome!
- Phrozen Heart for Phrozen Keep! All started with you, mate!
- A very special thanks to **Sanny8** for help with code editing.
- andsimo for the Hardcore Monster Changes Patch
- RandomOnions for being good support
- HarvestWombs for Code Editing tips
- Kingpin, AFJ, TrueMage, and everyone else from the PK forums! You guys are wonderful.


In short, if it wasn't for some really amaznig minds (not mine!) this mod wouuld not be possible. Thank you to everyone who contributes to Diablo 2 Modding! <3

*The description for Ironman mod was adopted from D2X: Darkness Weaves mod by RexxLaww.*
