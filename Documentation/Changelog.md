# 6.0.4
- Font16 Fixed
- Inventory background rgb(111)


# 6.0.3
- BUGFIX: Smithing Hammers show correct required parts for correct item
- MonDen up by 200
- MonUMax/N/H +1
- Monster ToHit +10
- Monster ToBlock +10
- More monsters auras
- More monster skills
- More monster on death skills (Be careful or an explosion might kill you)

# 6.0.2
- More difficulty added
- New monsters auras and skills on death
- More monsters missiles and skills (It's easy to die unless you play like an Ironman)
- enabled 3 socke gemwords on boots


# 6.0.1

- Made font thinner, fixed font offsets, no more overlapping glyphs.


# v6.0.0

**Note: new mod version will invalidate existing save files**

- Energy Cube + Energy Sphere (Animated Items)
- Combined Item Salvager and Magic Harvester into Energy Sphere (one item)
- Larger inventory
- Cleaner UI
- Quest, Skilltree, Waypoint background images
- New Char creation and char select screen
- Recolored Buttons
- 600+ new Gemwords
- 500+ new Runewords
- 81 new monster token replacements
- New monsters skills
- New Town1 map
- Magic and Rare Arrows & Bolts
- Magic and Rare Elemental Arrows & Bolts
- General difficulty raised to offset new items/runewords/gemwords
- New Demon Chest item graphic
- Removed /time based properties (Blizzard didn't code these correctly)
- Lots of New objects in Act 1 wilderness maps
- Socketed arrow and bolt quivers (gemwords possible)
- Socketed Rings and Amulets (gemwords possible)
- Socketed magic and rare charms (gemwords possible)
- New game loading screen animation
- New NPC graphics (Charsi, Akara, Gheed, Fara, etc.)

- Tons of new balance tweaks and changes after a few weeks of testing.



# 5.0.2
- Demon Chest Type Recipe Fix 

# 5.0.1
- Demon key size changed from 1x2 to 1x1 squares to match the demon chest size. Key was too big.

# 5.0.0
## Major Update!

### New Items:
- Demon Chests 
- Demon Keys

Cube a chest + key to open it and access items inside. The items demon chests will contain will be rebalanced as mod is playtested more.

- For new chars, at 10 souls vanquished, you will get a random demon key. "Huh? What is this? What does this do?"
- At 500 souls vanquished you get a demon chest! Keys and chest can also drop before 500 kills from champion/unique/superunique/bosses

### Demon Chests

![Demon Chest](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/DemonChest_sm.png)

#### 3 kinds of demonic chests

- Demon Chest Rare
- Demon Chest Set
- Demon Chest Unique

### Demon Keys

![Demon Key](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/DemonKey_sm.gif)

#### 3,000 different Demon Keys! 

Each key unlocks a chest, gives some random items according to chest quality, rare, set, unique, *if* you have the proper item level/char level.

![Demon Chest](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/DemonChest.jpg)
![Demon Key](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/DemonKey.jpg)
![Demon Chest + Key](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/DemonChestKey.jpg)


### New cube recipes!

- 3x, 6x, 9x potion upgrades in Cube
- 2x bolt = arrow
- 2x arrow = bolt
- 6 smithing hammer = new random smithing hammer

### Balance Changes
- Sorc/Necro toHit factor `+5`
- Stamina RunDrain slightly decreased `round(div/2 +5)`

### Remote Stash
- At 10,000 souls vanquished, you will get a portable stash item. Right click to access stash from anywhere outside town. Quality of life improvement at high game, so you don't have to keep running back to town, unless it's for a quest.

(Note: If this makes the game too easy upon play testing, it will be removed completely, as it may not fit the Ironman style of gameplay)



# 4.0.4
- Remote stash drops at 10,000 kills.
- Changed to official stash item graphic, 2x2 squares


# 4.0.3
- Updated stash item graphic to 3x3 chest


# 4.0.2

- Added Personal Stash as inventory item. 
- Right click to access stash from anywhere.
- New stash graphic.



# 4.0.1

- New Health Mana graphics. 
- Updated/cleaned general UI graphics. 
- Updated charmzone to be more obvious.

# 4.0.0

- Implemented Charm zone (lower bottom left of inventory, 6 squares across, 4 squares down, indicated by red border). Charms will only work in this area. 


# 3.5.30
- Cube size reduced to 2x2 again. 3x3 was too large.

# 3.5.29
- New graphics: Cube, Harvester, Smithing Orb

# 3.5.28
Renamed Orb of Forging to "Smithing Orb" to use with "Smithing Hammers"

# 3.5.27
- Cube, Harvester, Orb of Forging graphics update. 
(Caution, this might might make cube, harvester, and orb disappear from inventory since their sizes were changed. If this happens, just go to Gheed and get a new harvester. Go to Charsi and get a new Orb of Forging)


# 3.5.26
- Fixed Rare Javelin durability 1 bug

# 3.5.25

Orb of Forbing can now break down magic, rare, set, and uniqute items for even more parts.

Salvaging items is a tricky business. You may destroy the item and not get any parts at all. Or you could get a lot of parts. Salvaging items is always a gamble.

- For salvaging Magic: ROUND( (ITEL_LEVEL / 4) + 3 ) * 3.5
- For salvaging Rare: ROUND( (ITEL_LEVEL / 4) + 3 ) * 3.75
- For salvaging Set: ROUND( (ITEL_LEVEL / 4) + 3 ) * 4
- For salvaging Unique: ROUND( (ITEL_LEVEL / 4) + 3 ) * 4.25

2,000+ new cube recipes added for salvaging system.

For example: 
*Salvaging Magic Item iLVL 20*
- 20/4 = 5, 5+8 = 8, 8 * 3.5 = 28~ish item parts.

*Salvaging Unique Item iLVL 20*
- 20/4 = 5, 5+8 = 8, 8 * 4.25 = 40~ish item parts.



# 3.5.24
- Waypoint UI graphics fix (read white text)

# 3.5.23
- New UI graphics (requested by @andsimo)

# 3.5.22
- Tweaked monster skills for balanced gameplay/difficulty. (Only act 1 is partially done)

# 3.5.21
- Fixed +1 to an evil force on New Char's Cube due to empty row in skills.txt

# 3.5.20
- 3x any Smithing Hammers = New Random Smithing Hammer

- Smithing Hammer Treasureclasses Sorted by item levels and act. Higher quality hammers will drop in later acts.
- QuestSkills = 1,1,2 Book of skills,Den of evil, Izual
- 4 stat points per level (instead of 8)

- Monsters give 25% less experience (for slower leveling)
- Monsters have more on death skills, so be careful and don't die.
- Monster are slightly faster in walk, run, and AI frames
- mon run/walk velocity increased by 1
- mon toblock + 5%
- mon ctit + 5%

# 3.5.19
- Orb + magic item = always matriarchal javelin bug fixed


# 3.5.18
- Multiple items of same quality (up to 6x) can be harvested but with penalty. One unique item may give up to 89 points. But harvesting 6 uniques will give max 400 for example.

-Smithing Hammer sound changed to sword instead of potion.


# 3.5.17
- Moved bags/harvester/creation orb strings to spelldesc.
- Restructured item descriptions to looks better.

# 3.5.16
- 6,000+ Total Cube Recipes

## Magic Harvester Update! (Important, read this!)
Harvester no longer harvests a set amount of Essense Points. Instead the number of EP is now based on the item level and a forumla. 

- For Harvesting Magic: (ITEM_LVL / 4, + 7.5 ) * 1.75
- For Harvesting Rare: (ITEM_LVL / 4, + 7.5 ) * 2.25
- For Harvesting Set: (ITEM_LVL / 4, + 7.5 ) * 2.95
- For Harvesting Unique: (ITEM_LVL / 4, + 7.5 ) * 3.65

## Cost to create Magic, Set, Rare, Unique items:

- For Creating Rare: 300 Essence Points
- For Creating Set:  500 Essence Points
- For Creating Unique: 750 Essence Points

Item descriptions for bags/harvesters updated, to shorten strings, so it doesn't crash on mouseover.
Item descriptions changed to reflect new system of crafting.

- Smithing Hammers now show how many item parts they require.


# 3.5.14

- Added 1500 new cube recipes!
	- * Now every different item type/quality gives different item parts when salvaged.
	- * Low quality items will not salvage too many item parts.
	- * Normal quality items may salvage more item parts.
	- * High quality items may salvage even more parts.
	
	Formula is ```ROUND( (ITEL_LEVEL / 4) + 3 )``` for how up to how many item parts you can salvage.
	The cost to forge items is ```ROUND( ((ITEL_LEVEL / 4) + 3) * 1.5 )```

- Added Shamans to more act 1 outdoor areas
- Replaced poinson javalin from goatmen with poison bomb


# 3.5.13
- Shamans fixed in Cold Plains (weren't spawning).
- Less Key Drops

# 3.5.12
- Orb of Creation renamed to Izual's Orb of Forging

# 3.5.11 
- Smithing Cube Recipes Added

# 3.5.10
- TreasureClasses for Smithing Hammers Added
- Smithing Hammers dropping

# 3.5.9
- Smithing hammers added.


# 3.5.8
- New crafting system

## Orb of Creation

Cube with low/hi/normal quality white items to break the item into parts.
Cube with Smithing Scrolls to turn item parts into new items.

- Now every single item in the game serves some purpose! 
- Salvage all the low quality, high quality and normal (white) items in the game for item parts.
- Use item parts to create all sorts of new white items (then use the Magic Harvester's Essense Points to apply different properties to that item, turn it magic, rare, set, or unique).

![Orb of Creation](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/OrbOfCreation1.jpg)
![Orb of Creation](https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/OrbOfCreation2.jpg)


Todo: Create Smithing Hammers. 1 hammer for each item base code.




# 3.5.7 

## Kill Bonuses!  Survive the Hardcore Ironman mod as long as you can, and get bonses for staying alive!

- 10 Souls Vanquished: You will get 1 health potion for vanquishing the 10th monster's soul.
- 25 Souls Vanquished: Magic weapon + magic armor

Every 25 souls vanquished from now until 200, you will get one piece of magic equipment.
- 250 Souls Vanquished: Rare Armor + Rare Weapon
- 300,400,500,600,700,800,900,1000 Souls Vanquished: you will get one piece of rare equipment.

# Balance Changes:
- Monster are tougher. 
- Monsters are slightly faster, threat level slightly reduced so merc won't automatically target them. More challenge.



# 3.5.6
 
- Souls Vanquished counter font size fix




# 3.5.5 

- D2NewStat.dll + D2KillCounter.dll
- Souls Vanquished counter.



# 3.5.4

- Weapon/Armor durability increased.
- Little better potion drops.
- More magic item drops.
- Necro background update


# 3.5.3 

- Potion drops slightly increased
- Can store and extract up to six gems of the same quality in the gembags.
- Level lighting further reduced. The world is dark. Deal with it.
- Charstats adjusted according to class.
- Arrow stacks lowered drastically.
- Armor durability slightly increased.
- Gold is unspawnable.
- Monden up by 100
- More monster skills added for difficulty testing.

## New Cube Recipes:

- Stackable on item with repeat use, cannot be exploited, 
as item durability will run out. Only melee items:
- Any weapon + poison pot = dmg-pois 1-2 over 1 sec
- Any weapon + fire pot = dmg-fire 1-2 over 1 sec

Bugs: D2tweaks.dll update, golder filter doesn't work.

Todo: Need balance playtesting.

